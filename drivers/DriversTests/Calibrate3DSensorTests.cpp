#include <gtest/gtest.h>

#include "Drivers/Calibrate3DSensor.hpp"

TEST(Calibrate3DSensor, Calibrating_data_with_no_bias_and_a_scale_of_1)
{
	// Given
	Calibration calibration =
	{
		{ 0, 1.0f }, // x bias, scale
		{ 0, 1.0f }, // y bias, scale
		{ 0, 1.0f }  // z bias, scale
	};

	Calibrate3DSensor target(calibration);

	// When
	Raw3DSensorData raw = { 2, 3, 4 };
	Calibrated3DSensorData result = target.Calibrate(raw);

	// Then
	EXPECT_EQ(2, result.x);
	EXPECT_EQ(3, result.y);
	EXPECT_EQ(4, result.z);
}

TEST(Calibrate3DSensor, Calibrating_data_with_different_bias_and_a_scale_for_each_axis)
{
	// Given
	Calibration calibration =
	{
		{ -5, 2.0f }, // x bias, scale
		{ 10, 0.1f }, // y bias, scale
		{ 5,  3.0f }  // z bias, scale
	};

	Calibrate3DSensor target(calibration);

	// When
	Raw3DSensorData raw = { 1, 1, 1 };
	Calibrated3DSensorData result = target.Calibrate(raw);

	// Then
	EXPECT_EQ(-8.0f, result.x);
	EXPECT_EQ(1.1f,  result.y);
	EXPECT_EQ(18.0f, result.z);
}
