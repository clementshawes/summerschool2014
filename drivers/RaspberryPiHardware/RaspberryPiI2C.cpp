#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "RaspberryPiI2C.hpp"

RaspberryPiI2C::RaspberryPiI2C()
{
	file = open("/dev/i2c-0", O_RDWR);
	if(file < 0)
	{
		throw I2CUnableToOpenException();
	}
}

RaspberryPiI2C::~RaspberryPiI2C()
{
	close(file);
}

bool RaspberryPiI2C::SetAddress(unsigned char address)
{
	return (ioctl(file, I2C_SLAVE, address) >= 0);
}

bool RaspberryPiI2C::Read(void * buffer, int length)
{
	if(length > 0)
		return (read(file, buffer, length) == length);
	return false;
}

bool RaspberryPiI2C::Write(const void * buffer, int length)
{
	if(length > 0)
		return (write(file, buffer, length) == length);
	return false;
}
