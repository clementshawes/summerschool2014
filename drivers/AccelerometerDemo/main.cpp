#include <iostream>
#include <iomanip>
#include <sys/time.h>

#include "RaspberryPiHardware/RaspberryPiI2C.hpp"
#include "Drivers/BMA150Accelerometer.hpp"
#include "Drivers/ITG3200Gyroscope.hpp"
#include "Drivers/AK8975Compass.hpp"

int main(int /*argc*/, char* /*argv*/[])
{
	RaspberryPiI2C i2c;
	BMA150Accelerometer accelerometer(i2c);
	ITG3200Gyroscope gyroscope(i2c);
	AK8975Compass compass(i2c);

	timeval start;

	std::cout << std::fixed << std::setw(2) << std::setprecision(4);

	gettimeofday(&start, NULL);

	Raw3DSensorData direction = { 0, 0, 0 };
	compass.StartDirectionRead();
	while(1)
	{
		Raw3DSensorData acceleration = accelerometer.ReadAcceleration();
		Raw3DSensorData gyro = gyroscope.ReadGyroscope();
		
		if(compass.IsReadingComplete())
		{
			direction = compass.ReadDirection();
			compass.StartDirectionRead();
		}

		timeval current;

		gettimeofday(&current, NULL);

		const long seconds = current.tv_sec - start.tv_sec;
		const long useconds = current.tv_usec - start.tv_usec;
		const long elapsed = ((seconds) * 1000 + useconds/1000.0) + 0.5;

		std::cout << elapsed;
		std::cout << "\t" << acceleration.x << "\t" << acceleration.y << "\t" << acceleration.z;
		std::cout << "\t" << gyro.x << "\t" << gyro.y << "\t" << gyro.z;
		std::cout << "\t" << direction.x << "\t" << direction.y << "\t" << direction.z;
		std::cout << std::endl;
	}
	return 0;
}
