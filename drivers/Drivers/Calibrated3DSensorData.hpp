#pragma once

struct Calibrated3DSensorData
{
	float x;
	float y;
	float z;
};
