#pragma once

#include "HAL/I2C.hpp"
#include "Compass.hpp"

class AK8975Compass : public Compass
{
private:
	I2C& _i2c;

	void ReadRegisters(unsigned char startRegister, void* buffer, unsigned char numberOfRegistersToRead) const;
public:
	explicit AK8975Compass(I2C& i2c);

	virtual void StartDirectionRead() const;
	virtual bool IsReadingComplete() const;
	virtual Raw3DSensorData ReadDirection() const;


	unsigned char ReadDeviceID() const;
};
