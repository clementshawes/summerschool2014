#include "AK8975Compass.hpp"

const unsigned char AK8975Address = 0x0C;

AK8975Compass::AK8975Compass(I2C& i2c) :
	_i2c(i2c)
{
}

void AK8975Compass::ReadRegisters(unsigned char startRegister, void* buffer, unsigned char numberOfRegistersToRead) const
{
	_i2c.Write(&startRegister, sizeof(startRegister));
	_i2c.Read(buffer, numberOfRegistersToRead);
}

void AK8975Compass::StartDirectionRead() const
{
	_i2c.SetAddress(AK8975Address);

	const unsigned char startCommand[] = { 0x0A, 0x01 };
	_i2c.Write(startCommand, sizeof(startCommand));
}

bool AK8975Compass::IsReadingComplete() const
{
	_i2c.SetAddress(AK8975Address);

	unsigned char status1;

	ReadRegisters(0x02, &status1, sizeof(status1));

	return status1;
}

Raw3DSensorData AK8975Compass::ReadDirection() const
{
	Raw3DSensorData result;

	_i2c.SetAddress(AK8975Address);

	ReadRegisters(0x03, &result, sizeof(result));

	return result;
}

unsigned char AK8975Compass::ReadDeviceID() const
{
	unsigned char compassID;

	_i2c.SetAddress(AK8975Address);

	ReadRegisters(0x00, &compassID, sizeof(compassID));

	return compassID;
}
