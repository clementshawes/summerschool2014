#pragma once

#include "Raw3DSensorData.hpp"

class Gyroscope
{
public:
	virtual ~Gyroscope() { }
	virtual Raw3DSensorData ReadGyroscope() const = 0;
};
