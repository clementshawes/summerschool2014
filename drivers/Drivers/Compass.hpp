#pragma once

#include "Raw3DSensorData.hpp"

class Compass
{
public:
	virtual ~Compass() { }

	virtual void StartDirectionRead() const = 0;
	virtual bool IsReadingComplete() const = 0;
	virtual Raw3DSensorData ReadDirection() const = 0;
};
