#include "ITG3200Gyroscope.hpp"

const unsigned char ITG3200_ADDRESS = 0x68;

ITG3200Gyroscope::ITG3200Gyroscope(I2C& i2c) :
	_i2c(i2c)
{
	unsigned char initialiseCommand[] = { 0x16, 0x18 };
	_i2c.SetAddress(ITG3200_ADDRESS);
	_i2c.Write(initialiseCommand, sizeof(initialiseCommand));
}

short ITG3200Gyroscope::ConvertGyro(const unsigned char * buffer) const
{
	short result = buffer[0];
	result <<= 8;
	result |= buffer[1];
	return result;
}

Raw3DSensorData ITG3200Gyroscope::ReadGyroscope() const
{
	Raw3DSensorData result;

	const int NUMBER_GYRO_REGISTERS = 6;
	unsigned char registerAddress[] = { 0x1D };
	unsigned char gyroRegisters[NUMBER_GYRO_REGISTERS];

	_i2c.SetAddress(ITG3200_ADDRESS);
	_i2c.Write(registerAddress, sizeof(registerAddress));
	_i2c.Read(gyroRegisters, sizeof(gyroRegisters));

	result.x = ConvertGyro(&gyroRegisters[0]);
	result.y = ConvertGyro(&gyroRegisters[2]);
	result.z = ConvertGyro(&gyroRegisters[4]);

	return result;
}
