#pragma once

#include <exception>

#include "Raw3DSensorData.hpp"

class Accelerometer
{
public:
	virtual ~Accelerometer() { }
	virtual Raw3DSensorData ReadAcceleration() const = 0;
};

class AccelerometerException : public std::exception
{
};
